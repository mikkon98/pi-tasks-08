#include<iostream>
#include<string>
#include<time.h>
#include<stdlib.h>
#include "Matrix.h"

using namespace std;

int main()
{
	srand(time(0));
	setlocale(LC_ALL, "Russian");
	Matrix first(5, 5, "Matr 5x5");
	
	first.randomize(5, 5);

	cout << " Matr[1][1] = " << first[1][1] << " \n";

	cout << first;

	Matrix second(5, 5, "second");
	second.randomize(5, 5);
	cout << second;

	Matrix a = first*second;
	cout << second + first << first - second << a << first / 2 << second * 2;

	Matrix b(first);
	b -= first;
	cout << b;

	Matrix null_matrix(b.N(), b.M(), "null_matrix");
	cout << "������� " << null_matrix.getName() << " == " << b.getName() << " : "
		<< ((null_matrix == b) ? "true\n" : "false\n");
	cout << " ������� " << null_matrix.getName() << " != " << b.getName() << " : "
		<< ((null_matrix != b) ? "true\n" : "false\n");

	b.randomize(b.N(), b.M());
	b.setName("b");
	cout << b;
	cout << b.transposeIt();

	Matrix* toSomeFunc = new Matrix;

	return 0;
}