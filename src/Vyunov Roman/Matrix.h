using namespace std;

class Matrix_Cols {

private:
	double* values;
	int size;
	bool isOk;
public:
	Matrix_Cols(int size = 0)
	{
		this->isOk = size > 0 ? true : false;
		this->size = size;
		this->values = new double[size];
		for (int i = 0; i < size; i++)
			values[i] = 0;
	}
	~Matrix_Cols() 
	{
	}
	double& operator[](int i)
	{
		return values[i];
	}

	void randomize(int n)
	{
		bool check = n <= size && n >= 0;
		if (check)
		{
			for (int i = 0; i < n; i++)
				values[i] = rand() % 89 + 10;
		}
		else
		{
			cout << " ������������ ������ ������� "<<endl;
		}
	}
};

class Matrix {

	// ���� � ����� � ����� �������
	friend ostream& operator<<(ostream&, Matrix&);

	// ���������� == � !=
	friend bool operator ==(const Matrix& left, const Matrix& right);
	friend bool operator!=(const Matrix& left, const Matrix& right);

private:
	Matrix_Cols * matr;
	int cols, rows;
	string name;
	bool isOk;
public:

	// ����������� � �����������, �� ���������, �����������, ����������
	Matrix(int cols, int rows, string name)
	{
		this->name = name;
		this->isOk = cols&&rows ? isOk = true : isOk = false;
		this->cols = cols;
		this->rows = rows;
		matr = new Matrix_Cols[cols];
		for (int i = 0; i < cols; i++)
			matr[i] = Matrix_Cols(rows);
	}
	Matrix()
	{
		this->name = "NULL";
		this->isOk = false;
		this->cols = 1;
		this->rows = 1;
		matr = new Matrix_Cols[1];
		matr[0][0] = 0;
	}
	Matrix(const Matrix& m)
	{
		this->name = m.name;
		this->matr = m.matr;
		this->cols = m.cols;
		this->rows = m.rows;
		this->isOk = m.isOk;
	}
	~Matrix() 
	{
	}
	// ����� � cout, ���������� ���������� �������, ������� ��������, ����� �����
	void randomize(int n, int m)
	{
		bool check = (n <= cols && n >= 0) && (m <= rows && m >= 0);
		if (check)
		{
			for (int i = 0; i < n; i++)
				for (int j = 0; j < m; j++)
					matr[i][j] = rand() % 9 + 1;
		}
		else
		{
			cout << " ������������ ������ ������� "<< endl;
		}
	}
	int N() const 
	{ 
		return cols; 
	}
	int M() const 
	{ 
		return rows; 
	}
	void setName(string name) 
	{ 
		this->name = name; 
	}
	string getName() 
	{ 
		return name; 
	}
	// �������� ����� �������� � ��������
	Matrix operator+(const Matrix &m) const
	{
		if (isOk && m.isOk)
			if (cols == m.N() && rows == m.M())
			{
				Matrix tmp(cols, rows, name + "+" + m.name);
				for (int i = 0; i < cols; i++)
					for (int j = 0; j < cols; j++)
						tmp[i][j] = this->matr[i][j] + m.matr[i][j];
				return tmp;
			}
			else
			{
				cout << "������� ������ " << this->name << " � " << m.name << " �� ���������" << endl;
				return Matrix();
			}
		else
		{
			cout << "���� �� ������ �����"<<endl;
			return Matrix();
		}
	}
	Matrix operator-(const Matrix &m) const
	{
		if (isOk && m.isOk)
			if (cols == m.N() && rows == m.M())
			{
				Matrix tmp(cols, rows, name + "-" + m.name);
				for (int i = 0; i < cols; i++)
					for (int j = 0; j < cols; j++)
						tmp[i][j] = this->matr[i][j] - m.matr[i][j];
				return tmp;
			}
			else
			{
				cout << "������� ������ " << this->name << " � " << m.name << " �� ���������" << endl;
				return Matrix();
			}
		else
		{
			cout << "���� �� ������ �����"<< endl;
			return Matrix();
		}
	}
	Matrix operator*(const Matrix &m) const
	{
		if (rows != m.cols)
		{
			cout << "����� �������� " << name << " != ����� ����� " << m.name << endl;
			return Matrix();
		}
		Matrix tmp(cols, m.rows, name + "*" + m.name);
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < m.cols; j++)
				for (int k = 0; k < m.cols; k++)
					tmp[i][j] += (matr[i][j] * m.matr[k][j]);
		return tmp;
	}

	// �������� ����� �������� � ������
	Matrix operator+(int const x)
	{
		Matrix tmp(*this);
		for (int i = 0; i < cols; i++)
			for (int j = 0; j < rows; j++)
				tmp[i][j] += x;
		return tmp;
	}
	Matrix operator-(int const x)
	{
		Matrix tmp(*this);
		for (int i = 0; i < cols; i++)
			for (int j = 0; j < rows; j++)
				tmp[i][j] -= x;
		return tmp;
	}
	Matrix operator*(int const x)
	{
		Matrix tmp(*this);
		for (int i = 0; i < cols; i++)
			for (int j = 0; j < rows; j++)
				tmp[i][j] *= x;
		return tmp;
	}
	Matrix operator/(int const x)
	{
		if (x == 0)
		{
			cout << "������� ������� �� ����"<<endl;
			return Matrix();
		}
		Matrix tmp(*this);
		for (int i = 0; i < cols; i++)
			for (int j = 0; j < rows; j++)
				tmp[i][j] /= x;
		return tmp;
	}

	// �������� ������������ � ����������
	const Matrix& operator=(Matrix const &m)
	{
		if (&m != this)
		{
			if (cols != m.cols && rows != m.rows)
			{
				delete[] matr;
				cols = m.cols;
				matr = new Matrix_Cols[cols];
				for (int i = 0; i < cols; i++)
					matr[i] = Matrix_Cols(rows);
			}
			for (int i = 0; i < cols; i++)
				for (int j = 0; j < rows; j++)
					matr[i][j] = m.matr[i][j];
		}
		return *this;
	}
	Matrix& operator+=(const Matrix &m)
	{
		Matrix tmp = *this;
		tmp = tmp + m;
		return tmp;
	}
	Matrix& operator+=(int const x)
	{
		Matrix tmp = *this;
		tmp = tmp - x;
		return tmp;
	}
	Matrix& operator-=(const Matrix &m)
	{
		Matrix tmp = *this;
		tmp = tmp - m;
		return tmp;
	}
	Matrix& operator-=(int const x)
	{
		Matrix tmp = *this;
		tmp = tmp - x;
		return tmp;
	}
	Matrix& operator*=(const Matrix &m)
	{
		Matrix tmp = *this;
		tmp = tmp * m;
		return tmp;
	}
	Matrix& operator*=(int const x)
	{
		Matrix tmp = *this;
		tmp = tmp * x;
		return tmp;
	}
	Matrix& operator/=(int const x)
	{
		Matrix tmp = *this;
		tmp = tmp / x;
		return tmp;
	}
	// �������� ����������
	Matrix_Cols& operator[](int i)
	{
		return matr[i];
	}

	// ������ 
	
	Matrix transposeIt()
	{
		int n = M(), m = N();
		Matrix tmp(n, m, name);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				tmp[i][j] = matr[j][i];
		tmp.setName("�[" + tmp.getName() + "]");
		return tmp;
	}
	
};

ostream& operator <<(ostream &stream, Matrix& matr)
{
	if (matr.isOk != false)
	{
		int n = matr.N(), m = matr.M();
		stream << "������� " << matr.name << ":"<<endl;
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < m; j++)
				stream << matr[i][j]<<"\t";
			stream << endl;
		}
	}
	return stream;
}

bool operator==(Matrix const&a, Matrix const& b)
{
	if (a.rows != b.rows || a.cols != b.cols) return false;
	for (int i = 0; i < a.rows; i++)
		for (int j = 0; j < a.cols; j++)
			if (a.matr[i][j] != b.matr[i][j]) return false;
	return true;
}
bool operator!=(Matrix const& a, Matrix const& b) { return !(a == b); }